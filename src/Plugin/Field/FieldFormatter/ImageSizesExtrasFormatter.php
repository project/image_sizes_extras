<?php

namespace Drupal\image_sizes_extras\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image_sizes\Plugin\Field\FieldFormatter\ImageSizesPresetFormatter;

/**
 * Plugin implementation of the 'Example' formatter.
 *
 * @FieldFormatter(
 *   id = "image_sizes_extras_formatter",
 *   label = @Translation("Images sizes extras formatter"),
 *   field_types = {
 *     "image",
 *     "entity_reference"
 *   }
 * )
 */
class ImageSizesExtrasFormatter extends ImageSizesPresetFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState) {
    $element = parent::settingsForm($form, $formState);

    unset($element["load_invisible"]);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      $elements[$delta]['#theme'] = 'image_sizes_extras';
    }

    return $elements;
  }

}
