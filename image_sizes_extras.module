<?php

/**
 * @file
 * Primary module hooks for image_sizes_extras module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */

use Drupal\Component\Utility\NestedArray;

/**
 * Implements hook_theme().
 */
function image_sizes_extras_theme() {
  return [
    'image_sizes_extras' => [
      'variables' => [
        'entity' => NULL,
        'style' => NULL,
        'inline' => FALSE,
        'attributes' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_template_preprocess().
 */
function template_preprocess_image_sizes_extras(&$variables) {
  if (!isset($variables['style'], $variables['entity'])) {
    return;
  }

  /**
   * @var \Drupal\image_sizes\ImageSizesService
   */
  $service = \Drupal::service('image_sizes');
  $attributes = $service->getAttributes($variables['style'], $variables['entity'], TRUE);
  if (!is_array($variables['attributes'])) {
    $variables['attributes'] = [];
  }
  $attributes->setAttribute('fallback', $attributes['data-src-fallback']);
  $attributes->removeAttribute('class');
  $attributes->removeAttribute('data-src-fallback');
  !$attributes->hasAttribute('src') && $attributes->setAttribute('src', "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=");
  $variables['attributes'] = NestedArray::mergeDeep($variables['attributes'], $attributes->toArray());
  $variables['#attached']['library'] = [
    'image_sizes_extras/core',
  ];
}
