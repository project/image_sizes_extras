class i extends HTMLElement{#t;size;img=document.createElement("img");loaded=!1;get width(){return this.getAttribute("width")||"16"}get height(){return this.getAttribute("height")||"9"}get fallback(){return this.getAttribute("fallback")}get data(){const t=this.getAttribute("data-src")||"{}";try{const e=JSON.parse(t);return this.removeAttribute("data-src"),e}catch{console.error("Couldn't parse data-src attribute. Please check your JSON syntax.")}}get alt(){return this.getAttribute("alt")||"You should really set an alt attribute"}static styles=`
    img {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
      grid-column: 1 / -1;
      grid-row: 1 / -1;
      opacity: 0;
    }

    img.loaded {
      animation-name: var(--responsive-img-animation-name, fadeIn);
      animation-fill-mode: both;
      animation-duration: var(--responsive-img-animation-duration, 1s);
    }

    ::slotted(img) {
      transition: opacity var(--responsive-img-animation-duration, 1s) ease-in-out;
    }

    img.loaded + ::slotted(img) {
      opacity: 0;
    }

    @keyframes fadeIn {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
      }
    }
  `;constructor(){super(),this.size=this.offsetWidth,this.#t=new ResizeObserver(([t])=>{({width:this.size}=t.contentRect),this.update()}),this.attachShadow({mode:"open"})}connectedCallback(){this.render(),this.#t.observe(this)}disconnectedCallback(){this.#t.disconnect()}imgLoaded(){this.loaded||(this.loaded=this.img.complete,this.img.classList.add("loaded"))}inViewport(){const t=this.getBoundingClientRect();return t.top>=0&&t.left>=0&&t.bottom<=(window.innerHeight||document.documentElement.clientHeight)&&t.right<=(window.innerWidth||document.documentElement.clientWidth)}render(){Object.assign(this.img,{alt:this.alt,loading:this.inViewport()?"eager":"lazy",height:this.height,onload:()=>this.imgLoaded(),part:"image",sizes:`${Math.round(this.size)}px`,src:this.fallback,srcset:Object.entries(this.data).reduce((a,s)=>`${a} ${s[1]} ${s[0]}w,`,""),width:this.width});const t=document.createElement("style");t.textContent=i.styles;const e=document.createElement("slot");e.name="placeholder",this.shadowRoot?.append(t,this.img,e)}update(){this.img.sizes=`${Math.round(this.size)}px`}}window.customElements.define("responsive-img",i);
