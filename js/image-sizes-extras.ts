export class ResponsiveImage extends HTMLElement {
  #ro: ResizeObserver
  size: number
  img = document.createElement('img')
  loaded = false

  get width() {
    return this.getAttribute('width') || '16'
  }

  get height() {
    return this.getAttribute('height') || '9'
  }

  get fallback() {
    return this.getAttribute('fallback')
  }

  get data() {
    const data = this.getAttribute('data-src') || '{}'
    try {
      const json = JSON.parse(data)
      this.removeAttribute('data-src')
      return json
    }
    catch {
      console.error('Couldn\'t parse data-src attribute. Please check your JSON syntax.')
    }
  }

  get alt() {
    return this.getAttribute('alt') || 'You should really set an alt attribute'
  }

  static styles = `
    img {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
      grid-column: 1 / -1;
      grid-row: 1 / -1;
      opacity: 0;
    }

    img.loaded {
      animation-name: var(--responsive-img-animation-name, fadeIn);
      animation-fill-mode: both;
      animation-duration: var(--responsive-img-animation-duration, 1s);
    }

    ::slotted(img) {
      transition: opacity var(--responsive-img-animation-duration, 1s) ease-in-out;
    }

    img.loaded + ::slotted(img) {
      opacity: 0;
    }

    @keyframes fadeIn {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
      }
    }
  `

  constructor() {
    super()
    this.size = this.offsetWidth
    this.#ro = new ResizeObserver(([entry]) => {
      ({ width: this.size } = entry.contentRect)
      this.update()
    })
    this.attachShadow({ mode: 'open' })
  }

  connectedCallback() {
    this.render()
    this.#ro.observe(this)
  }

  disconnectedCallback() {
    this.#ro.disconnect()
  }

  imgLoaded() {
    if (this.loaded)
      return
    this.loaded = this.img.complete
    this.img.classList.add('loaded')
  }

  inViewport() {
    const rect = this.getBoundingClientRect()
    return (
      rect.top >= 0
      && rect.left >= 0
      && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
      && rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    )
  }

  render() {
    Object.assign(this.img, {
      alt: this.alt,
      loading: this.inViewport() ? 'eager' : 'lazy',
      height: this.height,
      onload: () => this.imgLoaded(),
      part: 'image',
      sizes: `${Math.round(this.size)}px`,
      src: this.fallback,
      srcset: Object.entries(this.data).reduce((acc, cur) => `${acc} ${cur[1]} ${cur[0]}w,`, ''),
      width: this.width,
    })

    const styles = document.createElement('style')
    styles.textContent = ResponsiveImage.styles

    const placeholder = document.createElement('slot')
    placeholder.name = 'placeholder'

    this.shadowRoot?.append(styles, this.img, placeholder)
  }

  /**
   * Update markup.
   */
  update() {
    this.img.sizes = `${Math.round(this.size)}px`
  }
}

window.customElements.define('responsive-img', ResponsiveImage)

declare global {
  interface HTMLElementTagNameMap {
    'responsive-img': ResponsiveImage
  }
}
