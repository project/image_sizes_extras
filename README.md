## Purpose

This module adds a custom template and js implementation for the 
[image sizes](https://www.drupal.org/project/image_sizes) module.

## What's different?
Instead of swapping the image url entirely, a browser native srcset is added. 
A resize observer sets the sizes attribute dynamically. Thus the browser can 
choose the correct image by itself. This also allows works with different DPI 
values. Just make sure that you preset is large enough.

E.g. If the image can be max 600px on your page then you should go up to 
1200px width to support 2x dpi screens.

## Potential Caveats
The preload image style width and height are used for the aspect ratio. 
This can cause slight layout shift when using image presets without 
fixed aspect ratios.
