import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    target: 'esnext',
    rollupOptions: {
      input: {
        'image-sizes-extras': './js/image-sizes-extras.ts',
      },
      output: {
        entryFileNames: () => {
          return '[name].js'
        },
      }
    },
    outDir: './js',
    emptyOutDir: false
  },
})
